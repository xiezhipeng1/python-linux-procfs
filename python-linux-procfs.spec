%global _description\
Python classes to extract information from the Linux kernel /proc files.

Name: python-linux-procfs
Version: 0.7.1
Release: 1
Summary: Linux /proc abstraction classes
License: GPLv2
URL: https://rt.wiki.kernel.org/index.php/Tuna
Source: https://git.kernel.org/pub/scm/libs/python/%{name}/%{name}.git/snapshot/%{name}-%{version}.tar.gz
BuildArch: noarch
BuildRequires: python3-devel python3-setuptools python3-six

%description %_description

%package -n python3-linux-procfs
Summary: %summary
%{?python_provide:%python_provide python3-linux-procfs}
Requires: python3-six
Obsoletes: python2-linux-procfs

%description -n python3-linux-procfs %_description

%prep
%autosetup -p1

%build
%py3_build

%install
%py3_install

%check
%{__python3} bitmasklist_test.py

%files -n python3-linux-procfs
%defattr(0755,root,root,0755)
%{_bindir}/pflags
%{python3_sitelib}/procfs/
%defattr(0644,root,root,0755)
%{python3_sitelib}/*.egg-info
%license COPYING

%changelog
* Thu Jan 19 2023 Zhipeng Xie <xiezhipeng1@huawei.com> - 0.7.1-1
- update to 0.7.1

* Wed Nov 02 2022 zhuofeng <zhuofeng2@huawei.com> - 0.7.0-1
- update to 0.7.0

* Tue Jun 21 2022 shixuantong <shixuantong@h-partners.com> - 0.6.3-3
- enable check

* Mon Jun 20 2022 shixuantong <shixuantong@h-partners.com> - 0.6.3-2
- fix file conflict

* Wed Nov 24 2021 liudabo <liudabo1@huawei.com> - 0.6.3-1
- upgrade version to 0.6.3

* Fri Oct 30 2020 wuchaochao <wuchaochao4@huawei.com> - 0.6.2-4
- Type:bufix
- CVE:NA
- SUG:NA
- DESC:remove python2

* Sat Sep 5 2020 shixuantong <shixuantong@huawei.com> -  0.6.2-3
- update Source0

* Wed Sep 02 2020 xinghe <xinghe1@huawei.com> - 0.6.2-2
- fix import of utilist

* Fri Jul 24 2020 tianwei <tianwei12@huawei.com> - 0.6.2-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: update to release 0.6.2

* Fri Sep 27 2019 yefei <yefei25@huawei.com> - 0.5.1-8
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: add file permissions

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.5.1-7
- Package init
